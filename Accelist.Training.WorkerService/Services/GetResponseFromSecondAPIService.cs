﻿using Coravel.Invocable;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.Training.WorkerService.Services
{
    public class GetResponseFromSecondAPIService : IInvocable
    {
        private readonly IHttpClientFactory ClientFactory;
        public GetResponseFromSecondAPIService(IHttpClientFactory clientFactory)
        {
            this.ClientFactory = clientFactory;
        }

        public async Task Invoke()
        {
            Log.Information("Run 2");
            var client = this.ClientFactory.CreateClient();
            var response = await client.GetAsync("http://dummy.restapiexample.com/api/v1/employees");
            var responseJson = await response.Content.ReadAsStringAsync();
            Log.Information(responseJson);
            
        }

       

    }
}
