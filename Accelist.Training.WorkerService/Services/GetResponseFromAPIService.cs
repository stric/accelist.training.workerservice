﻿using Coravel.Invocable;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.Training.WorkerService.Services
{
    public class GetResponseFromAPIService : IInvocable
    {
        private readonly IHttpClientFactory ClientFactory;
        public GetResponseFromAPIService(IHttpClientFactory clientFactory)
        {
            this.ClientFactory = clientFactory;
        }

        public async Task Invoke()
        {
            Log.Information("Run");
            var client = this.ClientFactory.CreateClient();
            var response = await client.GetAsync("http://dummy.restapiexample.com/api/v1/employees");
            var responseJson = await response.Content.ReadAsStringAsync();
            Log.Information(responseJson);
            
        }

       

    }
}
