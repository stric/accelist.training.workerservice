using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Accelist.Training.WorkerService
{
    public class SecondWorker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IHttpClientFactory HttpClient;

        public SecondWorker(ILogger<Worker> logger, IHttpClientFactory httpClient)
        {
            _logger = logger;
            this.HttpClient = httpClient;
            Log.Information("Construct 1");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                Log.Information("My Process 2");
                await Task.Delay(3000, stoppingToken);
            }
            //}
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            Log.Information("Worker Starting");
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Information("Worker Stoped");
            Log.CloseAndFlush();
            return base.StartAsync(cancellationToken);
        }
    }
}
