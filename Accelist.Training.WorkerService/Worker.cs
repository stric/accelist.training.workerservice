using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Accelist.Training.WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IHttpClientFactory HttpClient;
        public Worker(ILogger<Worker> logger, IHttpClientFactory httpClient)
        {
            _logger = logger;
            this.HttpClient = httpClient;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                
                Log.Information("Worker Processing");
                var client = this.HttpClient.CreateClient();

                var response = await client.GetAsync("http://dummy.restapiexample.com/api/v1/employees");
                var responseJson = await response.Content.ReadAsStringAsync();
                Log.Information(responseJson);
               

                await Task.Delay(1000, stoppingToken);
            }
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            Log.Information("Worker Starting");
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Information("Worker Stoped");
            Log.CloseAndFlush();
            return base.StartAsync(cancellationToken);
        }

    }
}
