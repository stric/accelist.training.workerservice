using Accelist.Training.WorkerService.Services;
using Coravel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using System;

namespace Accelist.Training.WorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", optional: false)
               .Build();
            Log.Logger = new LoggerConfiguration()
            .Enrich.FromLogContext()
            .Enrich.WithExceptionDetails()
            .ReadFrom.Configuration(configuration)
            .CreateLogger();

            try
            {
                var Host = CreateHostBuilder(args).Build();
                Host.Services.UseScheduler(scheduler =>
                {
                    scheduler.OnWorker("GetTask");
                    scheduler.Schedule<GetResponseFromAPIService>()
                    .EverySeconds(5)
                    .Weekday()
                    .PreventOverlapping("Key");
                    scheduler.Schedule<GetResponseFromSecondAPIService>()
                   .EverySeconds(5)
                   .Weekday()
                   .PreventOverlapping("Key");
                    //GetResponseFromAPIService and GetResponseFromSecondAPIService will share a dedicated pipeline/thread.

                    scheduler.OnWorker("PostTask");
                    scheduler.Schedule<PostToApiService>()
                    .EverySeconds(5)
                    .Weekday()
                    .PreventOverlapping("SecondKey");
                    //PostToApiService has it's own dedicated worker so it will not affect 
                    //the other tasks if it does take a long time to run.

                });
                Host.Run();
            }
            catch (Exception err)
            {
                Log.Error(err,err.Message);
            }
            finally
            {
                Log.CloseAndFlush();
            }

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddScheduler();
                    // Add this for invocable for invocable service
                    //services.AddTransient<GetResponseFromAPIService>();
                    //services.AddTransient<GetResponseFromSecondAPIService>();
                    //services.AddTransient<PostToAPIService>();


                    services.AddHostedService<Worker>();
                    //services.AddHostedService<SecondWorker>();
                    //Using Directly HttpClientFactory
                    services.AddHttpClient();



                });
               

    }
}
